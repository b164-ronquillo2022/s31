//Setup dependencies
const express = require('express');
const mongoose = require('mongoose');
//this allows us to use All the routes defined in "taskRoutes.js"
const taskRoutes = require('./routes/taskRoutes');

//Server Setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//Database connection
mongoose.connect('mongodb+srv://admin:admin@cluster0.alg8k.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


//Routes
app.use("/tasks", taskRoutes);
//http://localhost:3001/tasks/






app.listen(port, () => console.log(`Now listening to port ${port}`));


/*
Notes:
models > controllers > routes > index.js












*/


